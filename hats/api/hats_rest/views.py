from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Hats, LocationVO

# Create your views here.

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
        "id",
    ]

class HatListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "style_name",
        "location",
    ]
    def get_extra_data(self, o):
        return {"location": o.location.import_href}

class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style_name",
        "fabric",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(['GET'])
def api_list_locationvo(request):

    locations = LocationVO.objects.all()
    return JsonResponse(
        {'locations': locations},
        encoder=LocationVODetailEncoder,
    )


@require_http_methods(['GET', 'POST'])
def api_list_hats(request, location_id=None):

    if request.method == 'GET':
        if location_id is not None:
            hats = Hats.objects.filter(location=location_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {'hats': hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            location = LocationVO.objects.get(id=location_id)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location href"},
                status=400,
            )

        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(['GET', 'DELETE'])
def api_show_hats(request, hat_id):

    if request.method == 'GET':
        hat = Hats.objects.get(id=hat_id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )
    else:
            count, _ = Hats.objects.filter(id=hat_id).delete()
            return JsonResponse(
                {"deleted": count},
            )
