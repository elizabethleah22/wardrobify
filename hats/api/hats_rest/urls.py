from django.urls import path

from .views import (
    api_list_locationvo,
    api_list_hats,
    api_show_hats,
)

urlpatterns = [
    path('locations/', api_list_locationvo, name='api_list_locationvo'),
    path('location/<int:location_id>/hats/', api_list_hats, name='api_list_hats'),
    path('hats/', api_list_hats, name='api_create_hats'),
    path('hats/<int:hat_id>/', api_show_hats, name='api_show_hats'),
]
